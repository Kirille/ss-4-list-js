'use strict';

function Group () {
	var groupList = [];

	groupInit();
	
	function groupInit () {
		groupList.push(new Student('Kirill', '1993', 'M', 'kirillkozakceo', '380637467482', 'kirillkozakceo@gmail.com'));
		groupList.push(new Student('Nastya', '1991', 'F', 'kolomoets.anastasiya', '', ''));
		groupList.push(new Student('Chung', '', 'F', 'chung-alpha', '+380934637542', ''));
		groupList.push(new Student('Artur', '', 'M', 'art_smitt', '', ''));
		groupList.push(new Student('Dmitriy', '1988', 'M', 'pavlovsky_dima', '', ''));
		groupList.push(new Student('Petro', '', 'M', 'petr_artal', '', 'supershmell@gmail.com'));
	}
	
	this.add = function (Student) {
		groupList.push(Student);
	};

	this.getStudentsKeys = function () {
		return Object.keys(new Student('').toJSON());
	};
	
	this.getLength = function () {
		return groupList.length;
	};

	this.forEach = function (fn) {
		groupList.forEach(fn);		
	};

	this.toArray = function () {
		return groupList;
	};
	
	return this;
}