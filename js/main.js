/*
*	About project
*		This project create group list
*
*	About parameters
*		data - it is space where you can collect your information
*		container - contains our main DOM element
*
*	Last update: 7/05/2016
*/
'use strict';

document.addEventListener('DOMContentLoaded', init, false);

function init() {
	var parameters = {
		data: new Group(),
		container: document.getElementById('container')
	}

    new View(parameters);
}